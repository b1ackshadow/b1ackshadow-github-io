(function (document) {
  var toggle = document.querySelector(".sidebar-toggle");
  var sidebar = document.querySelector("#sidebar");
  var sidebarItem = document.querySelector(".sidebar-item");
  var checkbox = document.querySelector("#sidebar-checkbox");
  var portfolio = document.querySelector(".portfolio");
  var folder_icon = document.querySelector(".folder-icon");

  document.addEventListener(
    "click",
    function (e) {
      var target = e.target;
      console.log(target);

      if (
        !checkbox.checked ||
        sidebar.contains(target) ||
        target === checkbox ||
        target === toggle
      ) {
        return;
      }

      checkbox.checked = false;
      sidebarItem.scrollIntoView();
    },
    false
  );

  portfolio.addEventListener("mouseenter", (e) => {
    let classList = folder_icon.classList;
    if (!classList.contains("fa-folder-open")) {
      classList.add("fa-folder-open");
      classList.remove("fa-folder");
    }
  });

  portfolio.addEventListener("mouseleave", (e) => {
    let classList = folder_icon.classList;
    if (!classList.contains("fa-folder")) {
      classList.remove("fa-folder-open");
      classList.add("fa-folder");
    }
  });
})(document);
